import React from 'react';

class Individualdata extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }
  componentDidMount() {
    fetch(
      `https://api.kuvera.in/api/v3/funds/${this.props.match.params.id}.json`,
      {
        method: 'GET'
      }
    )
      .then(res => res.json())
      .then(item => this.setState({ data: item }));
  }
  render() {
    return (
      <div>
        {this.state.data.map(item => (
          <div key={item.code}>
            <h1
              className="d-flex justify-content-center"
              style={{ color: '#FFFFFF', backgroundColor: '#000000' }}
            >
              FUND DETAILS
            </h1>
            <ul className="list-group ">
              <li
                className="list-group-item d-flex justify-content-center"
                style={{ backgroundColor: '#f4f4f4' }}
              >
                CODE: {item.code}
              </li>
              <li
                className="list-group-item d-flex justify-content-center"
                style={{ backgroundColor: '#f4f4f4' }}
              >
                NAME: {item.name}
              </li>
              <li
                className="list-group-item d-flex justify-content-center"
                style={{ backgroundColor: '#f4f4f4' }}
              >
                FUND_NAME: {item.fund_name}
              </li>
            </ul>
          </div>
        ))}
      </div>
    );
  }
}
export default Individualdata;
