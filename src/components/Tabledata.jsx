import React from 'react';
import { Link } from 'react-router-dom';

function Tabledata(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => props.sortBy('name')}
            >
              Name
            </button>
          </th>

          <th>
            <div className="dropdown">
              <button
                className="btn btn-secondary dropdown-toggle"
                type="button"
                id="dropdownMenu2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Fund_Category
              </button>
              <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                {[
                  ...new Set(props.initialData.map(item => item.fund_category))
                ].map((item, i) => (
                  <button
                    key={i}
                    className="dropdown-item"
                    type="button"
                    onClick={() => props.filterData('fund_category', item)}
                  >
                    {item}
                  </button>
                ))}
              </div>
            </div>
          </th>

          <th>
            <div className="dropdown">
              <button
                className="btn btn-secondary dropdown-toggle"
                type="button"
                id="dropdownMenu2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Fund_Type
              </button>
              <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                {[...new Set(props.initialData.map(item => item.fund_type))].map(
                  (item, i) => (
                    <button
                      key={i}
                      className="dropdown-item"
                      type="button"
                      onClick={() => props.filterData('fund_type', item)}
                    >
                      {item}
                    </button>
                  )
                )}
              </div>
            </div>
          </th>

          <th>
            <div className="dropdown">
              <button
                className="btn btn-secondary dropdown-toggle"
                type="button"
                id="dropdownMenu2"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Plan
              </button>
              <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                {[...new Set(props.initialData.map(item => item.plan))].map(
                  (item, i) => (
                    <button
                      key={i}
                      className="dropdown-item"
                      type="button"
                      onClick={() => props.filterData('plan', item)}
                    >
                      {item}
                    </button>
                  )
                )}
              </div>
            </div>
          </th>

          <th>
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => props.sortByNum('returns', 'year_1')}
            >
              Return_year_1
            </button>
          </th>
          <th>
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => props.sortByNum('returns', 'year_3')}
            >
              Return_year_3
            </button>
          </th>
        </tr>
      </thead>
      {props.initialData.map(item => (
        <tbody key={item.code}>
          <tr>
            <td>
              <Link to={`/fund/${item.code}`}>{item.name}</Link>
            </td>
            <td>{item.fund_category}</td>
            <td>{item.fund_type}</td>
            <td>{item.plan}</td>
            <td>{item.returns.year_1}</td>
            <td>{item.returns.year_3}</td>
          </tr>
        </tbody>
      ))}
    </table>
  );
}
export default Tabledata;
