import React from 'react';
import Tabledata from '../components/Tabledata';
import FuzzySearch from 'fuzzy-search';

class Fund extends React.Component {
  constructor() {
    super();
    this.state = {
      fundData: [],
      initialData: [],
      sortType: {
        item: 'asc'
      },
      searchWord: ''
    };
  
    this.sortBy = this.sortBy.bind(this);
    this.sortByNum = this.sortByNum.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.filterData = this.filterData.bind(this);
  }

  componentDidMount() {
    fetch('https://api.kuvera.in/api/v3/funds.json', { method: 'GET' })
      .then(res => res.json())
      .then(data => this.setState({ fundData: data.slice(0, 100) , initialData: data.slice(0, 100)}));
  }

  sortBy = key => {
    this.setState({
      fundData: this.state.fundData.sort((a, b) => {
        let comparison = a[key].toUpperCase() > b[key].toUpperCase() ? 1 : -1;
        return this.state.sortType[key] === 'asc' ? comparison : -comparison;
      }),
      sortType: {
        [key]: this.state.sortType[key] === 'asc' ? 'desc' : 'asc'
      }
    });
  };

  sortByNum = (prop1, key) => {
    this.setState({
      fundData: this.state.fundData.sort((a, b) =>
        this.state.sortType[key] === 'asc'
          ? a[prop1][key] - b[prop1][key]
          : b[prop1][key] - a[prop1][key]
      ),
      sortType: {
        [key]: this.state.sortType[key] === 'asc' ? 'desc' : 'asc'
      }
    });
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    const searcher = new FuzzySearch(this.state.fundData, ['name'], {
      caseSensitive: false
    });
    const result = searcher.search(value);

    let initialData = [];
    initialData = result;
    this.setState({
      initialData: initialData
    });
  };

  filterData = (key1, key2) => {
    const result = this.state.fundData.filter(item => item[key1] === key2);
    this.setState({
      initialData: result
    });
  };

  render() {
    return (
      <div>
        <form className="form-inline ml-auto d-flex justify-content-center">
          <input
            className="form-control mr-sm-2"
            name="searchWord"
            value={this.state.searchWord}
            type="text"
            placeholder="search by name"
            onChange={this.handleChange}
          />
        </form>
        <Tabledata
          filterData={this.filterData}
          fundData={this.state.fundData}
          initialData={this.state.initialData}
          sortBy={this.sortBy}
          sortByNum={this.sortByNum}
        />
      </div>
    );
  }
}

export default Fund;
