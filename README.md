Call the below API to get all the funds in our MF universe. You will receive response of an array with below mentioned fields

https://api.kuvera.in/api/v3/funds.json

{"code":"TRSSG1-GR","name":"Taurus Starshare Growth Direct Plan","category":"Equity","reinvestment":"Z","fund_house":"TAURUSMUTUALFUND_MF","fund_type":"Equity","fund_category":"Multi Cap Fund","plan":"GROWTH","returns":{"year_1":-6.9062,"year_3":4.3664,"year_5":6.3022,"inception":8.4339,"date":"2019-07-25"},"volatility":14.9736}

The fund explore page must display a table with 100 entries containing following fields
- name of fund (clickable - should take you to a detailed page)
- fund_category
- fund_type
- plan
- year_1 returns
- year_3 returns

The table must be sortable based on all the column. name/fund_category/fund_type/plan : alphabetical, returns : value
provide drop-down for fund_category/fund_type/plan to filter out the result.
always display 100 top items from the list

## Fund detail page (/explore/xyz)
You can call the below API to get detail information of a specific fund.
https://api.kuvera.in/api/v3/funds/FR496-GR.json

Just display the key values that you think are important from the JSON into the view.