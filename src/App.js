import withFirebaseAuth from 'react-with-firebase-auth';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import firebaseConfig from './firebaseConfig';
import React from 'react';
import Fund from './components/Fund';
import Individualdata from './components/Individualdata';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAppAuth = firebaseApp.auth();
const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider()
};

const App = ({
  /** These props are provided by withFirebaseAuth HOC */

  signInWithGoogle,
  signOut,
  user
}) => (
  <Router>
    {user ? (
      <div>
        <p>Hello, {user.displayName}</p>
        <button className='btn btn-secondary' onClick={signOut}>Sign out</button>
        <Route exact path="/fund" render={() => <Fund />} />
        <Route path="/fund/:id" component={Individualdata} />
      </div>
    ) : (
      <div>
        <p>Please sign in.</p>
        <button className="btn btn-primary" onClick={signInWithGoogle}>
          Sign in with Google
        </button>
      </div>
    )}
  </Router>
);

export default withFirebaseAuth({
  providers,
  firebaseAppAuth
})(App);
